using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{

    public float Vida;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vida -= Time.deltaTime;

        if (Vida < 0)
        {
            Destroy(gameObject);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Juego : MonoBehaviour
{

    public GameObject player1;
    public GameObject player2;

    public int P1Life;
    public int P2Life;

    public GameObject gameOver;

    public GameObject[] p1image;
    public GameObject[] p2image;


    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        if (P1Life <= 0)
        {
            player1.SetActive(false);


            //anim.SetBool("dead", true);

            gameOver.SetActive(true);
        }

        if (P2Life <= 0)
        {
            player2.SetActive(false);
            //anim.SetBool("dead", true);
            gameOver.SetActive(true);
        }
    }

    public void Vidap1()
    {
        P1Life -= 1;

        for(int i=0; i < p1image.Length;i++)
        {
            if (P1Life > i)
            {
                p1image[i].SetActive(true);
            }
            else
            {
                p1image[i].SetActive(false);

            }
        }

    }

    public void Vidap2()
    {
        P2Life -= 1;
        for (int i = 0; i < p2image.Length; i++)
        {

            if (P2Life > i)
            {
                p2image[i].SetActive(true);
            }
            else
            {
                p2image[i].SetActive(false);

            }
        }

    }
}


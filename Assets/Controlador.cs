using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Controlador : MonoBehaviour
{
    public float Velocidad;
    public float FuerzaSalto;

    public KeyCode Izquierda;
    public KeyCode Derecha;
    public KeyCode Saltar;
    public KeyCode Disparar;

    private Rigidbody2D theRB;

    public Transform groundCheckPoint;
    public float GroundCheckRadius;
    public LayerMask WhatisGround;

    public bool isGrounded;

    public GameObject bullet;
    public Transform throwPoint;


    private Animator anim;


    void Start()
    {
        theRB = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, GroundCheckRadius, WhatisGround);
        if (Input.GetKey(Izquierda))
        {
            theRB.velocity = new Vector2(-Velocidad, theRB.velocity.y);
        }
        else if(Input.GetKey(Derecha)){
            theRB.velocity = new Vector2(Velocidad, theRB.velocity.y);
        }
        else
        {
            theRB.velocity = new Vector2(0, theRB.velocity.y);
        }

        if (Input.GetKeyDown(Saltar) && isGrounded)
        {
            theRB.velocity = new Vector2(theRB.velocity.x, FuerzaSalto);

        }
        if (Input.GetKeyDown(Disparar))
        {

            GameObject bulletClone =Instantiate(bullet, throwPoint.position, throwPoint.rotation);

            bulletClone.transform.localScale = transform.localScale;

           // bulletClone.transform.SetParent(theRB.transform, true);

        }



        if (theRB.velocity.x < 0){
            transform.localScale = new Vector3(-0.19f, 0.19f, 0.19f);
        }else if(theRB.velocity.x > 0)
        {
            transform.localScale = new Vector3(0.19f, 0.19f, 0.19f);

        }

        anim.SetFloat("speed", Mathf.Abs(theRB.velocity.x));
        //anim.SetBool("dead", isGrounded);
    }


}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{

    public float bulletspeed;

    private Rigidbody2D theRB;

    // Start is called before the first frame update
    void Start()
    {
        theRB = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        theRB.velocity = new Vector3(bulletspeed * transform.localScale.x, 0);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player1")
        {
            FindObjectOfType<Juego>().Vidap1();
        }

        if (other.tag == "Player2")
        {
            FindObjectOfType<Juego>().Vidap2();
        }
        Destroy(gameObject);
    }

}
